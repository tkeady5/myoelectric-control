
#define EMG_THRESHOLD 10  // 0-1023
#define DOUBLE_CONTR_TIME 1000 // ms from rising edge of first pulse to rising edge of second pulse
// Reason for counting it from rising not falling: if wait for it from the release of the pulse, then release may be
// accidental because when holding nerve impulse decreases even if you think youre holding it
#define LATENCY 200 // ms from first detection of rising/falling edge during which no variable change happens, must be < DCT
#define RESETTING_THRESH 9 //0-1023
// If EMG signal is held after state/grip change, controller will wait until signal drops below this threshold
// to reset the variables so that they actually represent the EMG signal coming in (important that it is
// less than the regular threshold because the signal diminishes even if patient is holding, and should be > 0
// because there might be noise)


#include <Servo.h>
Servo thumb;
Servo index;
Servo mid_ring_pink;


unsigned long time = 0;
unsigned long latencyStart = 0;
int reading = 0;

bool pulseOneActive = false;  // True from rising edge til falling edge of first pulse (then set to false)
unsigned long pulseOneTime = 0;  // Rising edge of first pulse
bool pulseOneEnded = false; // True after falling edge of first pulse

bool resetting = false;  // need to wait for EMG pulse to end to reset vars properly
int resettingIndicator = 12;  // resetting indicator LED

bool isOpen = true;
int grip = 0;



 ///////////////////////////////////////////////////////// testing LEDs
                                           int POA = 3;
                                           int POE = 4;
                                           int DCT = 5;   // is it within double contratction time
                                           int g1 = 6;
                                           int g2 = 7;
                                           int g3 = 8;
                                           int io = 2;    // is open?
 ///////////////////////////////////////////////////////// testing




void setup() {
  // put your setup code here, to run once:
  pinMode(A0, INPUT);


  thumb.attach(9);
  index.attach(10);
  mid_ring_pink.attach(11);


 ///////////////////////////////////////////////////////// testing
                                           pinMode(POA, OUTPUT);
                                           pinMode(POE, OUTPUT);
                                           pinMode(DCT, OUTPUT);
                                           pinMode(g1, OUTPUT);
                                           pinMode(g2, OUTPUT);
                                           pinMode(g3, OUTPUT);
                                           /*
                                           // motors on pins 9 10 11
                                           pinMode(9, OUTPUT);  //thumb
                                           pinMode(10, OUTPUT);  //index
                                           pinMode(11, OUTPUT);  //others
                                           */
                                           pinMode(io, OUTPUT);
 ///////////////////////////////////////////////////////// testing


  stateChange(false, 0);

  digitalWrite(resettingIndicator, HIGH);
  delay(DOUBLE_CONTR_TIME + 1); // to make sure millis() gets far enough away from times
  digitalWrite(resettingIndicator, LOW);  
}



void loop() {
  // put your main code here, to run repeatedly:

  reading = analogRead(A0);
  time = millis();  // only here for testing, to make more efficient can move back to after resetting else

  if (resetting) {
    if (reading > RESETTING_THRESH) {
      digitalWrite(resettingIndicator, HIGH);
    } else {    // Actual reset takes place here
      pulseOneActive = false;
      pulseOneEnded = false;
      digitalWrite(resettingIndicator, LOW);
      resetting = false;
    }
    
  } else {
    
    //time = millis();


    // start PO detection if statement
    if ( (reading > EMG_THRESHOLD) && !pulseOneEnded && ( (time - latencyStart) > LATENCY) ) {    // First pulse detected

      if (!pulseOneActive) {
        pulseOneActive = true;
        pulseOneTime = time;
        latencyStart = time;
      }

    }  // end PO detection if statement



    // start PO ending if statement
    if (pulseOneActive && ( reading < EMG_THRESHOLD) && ( (time - latencyStart) > LATENCY) ) {
      pulseOneActive = false;
      pulseOneEnded = true;      // at no time will these ever both be true
    }  // end PO ending if statement


    // start waiting if statement (DOUBLE_CONTR_TIME has passed)
    if ( ( (time - pulseOneTime) > DOUBLE_CONTR_TIME ) && (pulseOneEnded || pulseOneActive) && ( (time - latencyStart) > LATENCY) ) {

      // STATE CHANGE STATE CHANGE STATE CHANGE STATE CHANGE
      stateChange(isOpen, grip);
      isOpen = !isOpen;
      // RESET RESET RESET RESET
      resetting = true;


    }



    // start PT detection if statement
    if ( (reading > EMG_THRESHOLD) && pulseOneEnded && ( (time - latencyStart) > LATENCY) ) {    // Second pulse detected

      // getting here outside DOUBLE_CONTR_TIME will be precluded by the waiting if statement above
      // GRIP SWITCH GRIP SWITCH GRIP SWITCH GRIP SWITCH
      if (grip == 0) {
        ++grip;
      } else if (grip == 1) {
        ++grip;
      } else if (grip == 2) {
        grip = 0;
      }

      // RESET RESET RESET RESET
      resetting = true;
    }

  }  // end resetting if
  
  //////////////////////////////////////////////////////////////////////  testing
                                           
                                           if (pulseOneActive) {
                                             digitalWrite(POA, HIGH);
                                           } else {
                                             digitalWrite(POA, LOW);
                                           }
                                           if (pulseOneEnded) {
                                             digitalWrite(POE, HIGH);
                                           } else {
                                             digitalWrite(POE, LOW);
                                           }
                                           if ( (time - pulseOneTime < DOUBLE_CONTR_TIME) && (pulseOneEnded || pulseOneActive) ) {
                                             digitalWrite(DCT, HIGH);
                                           } else {
                                             digitalWrite(DCT, LOW);
                                           }
                                           if (pulseOneEnded) {
                                             digitalWrite(POE, HIGH);
                                           } else {
                                             digitalWrite(POE, LOW);
                                           }
                                           if (grip == 0) {
                                             digitalWrite(g1, HIGH);
                                             digitalWrite(g2, LOW);
                                             digitalWrite(g3, LOW);
                                           } else if (grip == 1) {
                                             digitalWrite(g1, LOW);
                                             digitalWrite(g2, HIGH);
                                             digitalWrite(g3, LOW);
                                           } else if (grip == 2) {
                                             digitalWrite(g1, LOW);
                                             digitalWrite(g2, LOW);
                                             digitalWrite(g3, HIGH);
                                           }
                                           
                                           if (isOpen) {
                                             digitalWrite(io, HIGH);
                                           } else {
                                             digitalWrite(io, LOW);
                                           }
                                           
  //////////////////////////////////////////////////////////////////////  testing
  
  
  
  
  
  
  
  
}  // end loop



void stateChange(bool isOp, int g) {
  if (isOp) {
    if (g == 0) {    // Currently is a fist
      thumb.write(160);
      index.write(20);
      mid_ring_pink.write(20);
    } else if (g == 1) {    // Currently is a point
      thumb.write(20);
      index.write(160);
      mid_ring_pink.write(20);
    } else if (g == 2) {    // Currently is a pincher
      thumb.write(20);
      index.write(20);
      mid_ring_pink.write(160);
    }
  } else {    // Is open hand
    thumb.write(20);
    index.write(20);
    mid_ring_pink.write(20);
  }

} 

/*
void stateChange(bool isOp, int g) {
  if (isOp) {
    if (g == 0) {    // Currently is a fist
      digitalWrite(9, HIGH);
      digitalWrite(10, HIGH);
      digitalWrite(11, HIGH);
    } else if (g == 1) {    // Currently is a point
      digitalWrite(9, LOW);
      digitalWrite(10, HIGH);
      digitalWrite(11, LOW);
    } else if (g == 2) {    // Currently is a pincher
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, HIGH);
    }
  } else {    // Is open hand
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);
  }

} 
*/
